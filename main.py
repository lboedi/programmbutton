#!/usr/bin/env python
# -*- coding: UTF-8 -*-


from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.lang import Builder
import os
from functools import partial

#load kv file
Builder.load_file('main.kv')

class MainGrid(Widget):
    loaded=False
    def __init__(self, productv, **kwargs):
        super(MainGrid, self).__init__(**kwargs)
        self.productv=productv
    
    def load(self, *kwargs):
        if self.loaded: 
             return
        counter=0
        for image in os.listdir('data/cat'):
             if not image.startswith('.'):
                button=Button(background_down='data/cat/'+image,
                              background_normal='data/cat/'+image,
                              border=(0,0,0,0))
                button.bind(on_release=partial(self.productv.load, counter))
                button.bind(on_release=self.unload)
                self.maingrid.add_widget(button) 
                counter+=1
        self.loaded=True
        
    def unload(self, *kwargs):
        if not self.loaded:
            return
        self.maingrid.clear_widgets()
        self.loaded=False

class ProductView(Widget):     
         loaded=False
    def  my_function(self, *kwargs):
         if self.loaded: 
            return
            counter=0
         for image in os.listdir('data/product'):
            button.bind(on_release=my_function)
            self.root.add_widget(big_image)
            counter+=1
         self.loaded=True
        
    def unload(self, *kwargs):
        if not self.loaded:
           return
        self.clear_widgets()
        self.loaded=False

        
        
class TestApp(App):
    def build(self):
        root = Widget()
        productv = ProductView()
        maing = MainGrid(productv=productv)
        maing.load()
        productv.maingrid=maing
        root.add_widget(productv)
        root.add_widget(maing)
        return root
TestApp().run()